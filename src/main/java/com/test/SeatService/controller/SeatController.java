package com.test.SeatService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.SeatService.model.Seat;
import com.test.SeatService.service.SeatService;

@RestController
@RequestMapping(value = "/seat")
public class SeatController {
	
	@Autowired
	private SeatService service;

	@GetMapping(value="/{busId}/{customerId}")
	public Seat[] getSeatForBus(@PathVariable(name="busId", required=false) Integer busId,@PathVariable(name="customerId", required=true) Integer customerId){
		
		return service.getSeatByBusId(busId,customerId);
	}
	
}
