package com.test.SeatService.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.SeatService.model.Seat;

@Service
public class SeatService {

	List<Seat> seatList = new ArrayList<>(Arrays.asList(
			new Seat(1,"passenger1",true,1,1),
			new Seat(2,"passenger2",true,1,1),
			new Seat(1,"passenger3",true,2,1),
			new Seat(2,"passenger4",true,2,2),
			new Seat(3,"passenger5",true,2,3),
			new Seat(3,"passenger6",true,1,1),
			new Seat(4,"passenger7",true,1,2)
			));
	
	
	public Seat[] getSeatByBusId(Integer busId, Integer customerId){
		List<Seat> sList = new ArrayList<>();
		for(Seat seat: seatList) {
			if(seat.getBusId().equals(busId) && seat.getCustomerId().equals(customerId))
				sList.add(seat);
		}
		Seat[] seatarray = new Seat[sList.size()];
		return sList.toArray(seatarray);
	}
	
}
