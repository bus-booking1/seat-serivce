package com.test.SeatService.model;

public class Seat {

	
	private Integer seatId;
	private String passengerName;
	private Integer busId; 
	private Integer customerId;
	private Boolean gender;
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getBusId() {
		return busId;
	}

	public void setBusId(Integer busId) {
		this.busId = busId;
	}

	//true for male, false for female
	
	
//	public Seat() {
//		
//	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public Seat(Integer seatId, String passengerName, Boolean gender,Integer busId,Integer customerId) {
		super();
		this.seatId = seatId;
		this.passengerName = passengerName;
		this.gender = gender;
		this.busId = busId;
		this.customerId = customerId;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public  Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	
	
	
}
